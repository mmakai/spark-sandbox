import {autoinject} from 'aurelia-framework';
import {NotebookService} from './notebook-service';
import 'jquery';

@autoinject
export class CreateNote {

  root: null;

  name: string;

  notebookService: NotebookService;

  constructor(notebookService: NotebookService) {
    this.notebookService = notebookService;
  }

  attached() {
    $(this.root).on('keypress', e => {
      if (e.which == 13) {
        $(this.root).modal('hide');
        this.ok();
      }
    });
    $(this.root).on('shown.bs.modal', () => $(this.root).find('input').focus());
  }

  detached() {
    $(this.root).off('keypress');
    $(this.root).off('shown.bs.modal');
  }

  show() {
    this.name = '';
    $(this.root).modal();
  }

  ok() {
    // Empty name is not stored.
    this.notebookService.createNote(this.name || null);
  }
}
