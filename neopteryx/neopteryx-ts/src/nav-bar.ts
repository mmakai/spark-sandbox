import {autoinject, bindable} from 'aurelia-framework';
import {EventAggregator, Subscription} from 'aurelia-event-aggregator';
import {Router} from 'aurelia-router';
import {NoteInfoM} from './note-info-m';
import {CreateNote} from './create-note';
import {NotebookService} from './notebook-service';

@autoinject
export class NavBar {

  eventAggregator: EventAggregator;

  notebookService: NotebookService;

  createNote: CreateNote;

  connectionState: ConnectionState = ConnectionState.Yellow;

  @bindable router: Router = null;

  connectionStatusSubscription: Subscription;

  getNoteInfosSubscription: Subscription;

  noteInfos: NoteInfoM[] = null;

  constructor(eventAggregator: EventAggregator, notebookService: NotebookService) {
    this.eventAggregator = eventAggregator;
    this.notebookService = notebookService;
  }

  bind() {
    this.connectionStatusSubscription = this.eventAggregator.subscribe('connectionstatus', payload => {
      if (payload === 'open') {
        this.connectionState = ConnectionState.Green;
      } else if (payload === 'close') {
        this.connectionState = ConnectionState.Red;
      } else {
        this.connectionState = ConnectionState.Yellow;
      }
    });
    this.getNoteInfosSubscription = this.eventAggregator.subscribe('getNoteInfos', payload => {
      this.noteInfos = payload;
    });
    this.notebookService.connect();
  }

  unbind() {
    this.connectionStatusSubscription.dispose();
    this.getNoteInfosSubscription.dispose();
  }

  showCreateNote() {
    this.createNote.show();
  }
}

export enum ConnectionState {

  Red, Yellow, Green
}
