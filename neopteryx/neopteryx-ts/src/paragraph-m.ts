export interface ParagraphM {

  id: string;

  text?: string;

  // Volatile.
  editorText?: string;
}
