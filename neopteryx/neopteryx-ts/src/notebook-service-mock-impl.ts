import {EventAggregator} from 'aurelia-event-aggregator';
import * as LogManager from 'aurelia-logging';
import {NoteInfoM} from './note-info-m';
import {NoteM} from './note-m';
import {ParagraphM} from './paragraph-m';

export class NotebookServiceMockImpl {

  logger = LogManager.getLogger('NotebookServiceMockImpl');

  eventAggregator: EventAggregator;

  noteInfos: NoteInfoM[] = [];

  // TODO This should be a map.
  notes: NoteM[] = [];

  constructor(eventAggregator: EventAggregator) {
    this.eventAggregator = eventAggregator;
    this.noteInfos.push({id: '28', name: 'apple'});
    this.noteInfos.push({id: '29', name: 'orange'});
    this.noteInfos.push({id: '30'});
    this.notes['28'] = {id: '28', name: 'apple', paragraphs: [{
        id: '' + Math.random(),
        text: `function foo(items) {
  var x = "All this is syntax highlighted";
  return x;
}`
      }, {
        id: '' + Math.random(),
        text: 'apple2 apple'
      }]
    };
    this.notes['29'] = {id: '29', name: 'orange', paragraphs: []};
    this.notes['30'] = {id: '30'};
  }

  connect() {
    if (this.isConnected()) {
      this.eventAggregator.publish('connectionstatus', 'open');
      this.eventAggregator.publish('getNoteInfos', this.noteInfos);
      this.getNoteInfos();
    }
  }

  close() {
  }

  getHomeNote() {
  }

  createNote(noteName: string) {
    let id = '' + Math.random();
    this.notes[id] = {id: id, name: noteName, paragraphs: [{
        id: '' + Math.random()
      }]
    };
    this.noteInfos.push({id: id, name: noteName});
  }

  deleteNote(noteId: string) {
  }

  cloneNote(noteIdToClone: string, newNoteName: string) {
  }

  getNoteInfos() {
  }

  getNote(noteId: string) {
    this.eventAggregator.publish('getNote', this.notes[noteId]);
  }

  moveParagraph(noteId: string, paragraphId: string, newIndex: number) {
    let paragraphs = this.notes[noteId].paragraphs;
    let index = paragraphs.findIndex(p => p.id == paragraphId);
    if (index >= 0 && index != newIndex) {
      // NOTE Simple swap of paragraphs does not work, as repeat.for is not
      // observing that. Hence we first delete a paragraph and insert it
      // at the new index.
      let paragraph = this.notes[noteId].paragraphs.splice(index, 1);
      this.notes[noteId].paragraphs.splice(newIndex, 0, paragraph[0]);
    }
  }

  insertParagraph(noteId: string, newIndex: number) {
    let paragraph: ParagraphM = {id: '' + Math.random()};
    if (this.notes[noteId].paragraphs == null) {
      this.notes[noteId].paragraphs = [];
    }
    this.notes[noteId].paragraphs.splice(newIndex, 0, paragraph);
  }

  saveParagraph(noteId: string, paragraphId: string) {
    let paragraphs = this.notes[noteId].paragraphs;
    let index = paragraphs.findIndex(p => p.id == paragraphId);
    paragraphs[index].text = paragraphs[index].editorText;
  }

  removeParagraph(noteId: string, paragraphId: string) {
    let paragraphs = this.notes[noteId].paragraphs;
    let index = paragraphs.findIndex(p => p.id == paragraphId);
    if (index >= 0) {
      this.notes[noteId].paragraphs.splice(index, 1);
    }
  }

  isConnected(): boolean {
    return true;
  }
}
