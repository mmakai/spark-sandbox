import {ParagraphM} from './paragraph-m';

export interface NoteM {

  id: string;

  name?: string;

  paragraphs?: ParagraphM[];
}
