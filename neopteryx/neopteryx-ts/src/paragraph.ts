import {autoinject, bindable, BindingEngine, Disposable} from 'aurelia-framework';
import {NotebookService} from './notebook-service';
import 'ace';
import 'ace/theme-chrome';
import 'ace/mode-scala';
import {ParagraphM} from './paragraph-m';

@autoinject
export class Paragraph {

  @bindable noteId: string;

  @bindable index: number;

  @bindable first: boolean;

  @bindable last: boolean;

  @bindable paragraph: ParagraphM = null;

  bindingEngine: BindingEngine

  notebookService: NotebookService;

  textSubscription: Disposable; 

  constructor(bindingEngine: BindingEngine, notebookService: NotebookService) {
    this.bindingEngine = bindingEngine;
    this.notebookService = notebookService;
  }

  attached() {
    let aceEditor = ace.edit(this.editor);
    this.aceEditor = aceEditor;
    aceEditor.setTheme('ace/theme/chrome');
    aceEditor.session.setMode('ace/mode/scala');
    aceEditor.renderer.setShowGutter(true);
    this.autoAdjustEditorHeight();
    aceEditor.getSession().on('change', (e, editSession) => this.autoAdjustEditorHeight());
    this.textSubscription = this.bindingEngine
        .propertyObserver(this.paragraph, 'text')
        .subscribe(e => {
      this.paragraph.editorText = this.paragraph.text || '';
      this.aceEditor.setValue(this.paragraph.editorText, 1);
    });
    this.aceEditor.getSession().on('change', e => {
      this.paragraph.editorText = this.aceEditor.getValue();
    });
    if (!this.paragraph.editorText) {
      this.paragraph.editorText = this.paragraph.text || '';
    }
    this.aceEditor.setValue(this.paragraph.editorText, 1);
  }

  detached() {
    this.textSubscription.dispose(); 
  }

  // TODO listen to window resize.
  autoAdjustEditorHeight() {
    let aceEditor = this.aceEditor;
    let height = aceEditor.getSession().getScreenLength() * aceEditor.renderer.lineHeight +
        aceEditor.renderer.scrollBar.getWidth();
    this.editor.style.height = `${height}px`;
    aceEditor.resize();
  }

  save() {
    this.notebookService.saveParagraph(this.noteId, this.paragraph.id, this.text);
  }

  paragraphUp() {
    this.notebookService.moveParagraph(this.noteId, this.paragraph.id, this.index - 1);
  }

  paragraphDown() {
    this.notebookService.moveParagraph(this.noteId, this.paragraph.id, this.index + 1);
  }

  removeParagraph() {
    this.notebookService.removeParagraph(this.noteId, this.paragraph.id);
  }
}
