import {autoinject} from 'aurelia-framework';
import {EventAggregator} from 'aurelia-event-aggregator';
import {NotebookServiceMockImpl} from './notebook-service-mock-impl';
import {NotebookServiceWebSocketImpl} from './notebook-service-websocket-impl';

@autoinject
export class NotebookService {

  //notebookServiceImpl: NotebookServiceMockImpl;
  notebookServiceImpl: NotebookServiceWebSocketImpl;

  constructor(eventAggregator: EventAggregator) {
    //this.notebookServiceImpl = new NotebookServiceMockImpl(eventAggregator);
    this.notebookServiceImpl = new NotebookServiceWebSocketImpl(eventAggregator);
  }

  connect() {
    this.notebookServiceImpl.connect();
  }

  close() {
    this.notebookServiceImpl.close();
  }

  getHomeNote() {
    this.notebookServiceImpl.getHomeNote();
  }

  createNote(noteName: string) {
    this.notebookServiceImpl.createNote(noteName);
  }

  deleteNote(noteId: string) {
    this.notebookServiceImpl.deleteNote(noteId);
  }

  cloneNote(noteIdToClone: string, newNoteName: string) {
    this.notebookServiceImpl.cloneNote(noteIdToClone, newNoteName);
  }

  getNoteInfos() {
    this.notebookServiceImpl.getNoteInfos()
  }

//  reloadAllNotesFromRepo() {
//    this.sendJSON({op: 'RELOAD_NOTES_FROM_REPO'});
//  }

  getNote(noteId: string) {
    this.notebookServiceImpl.getNote(noteId);
  }

//  updateNote(noteId: string, noteName: string, noteConfig) {
//    this.sendJSON({op: 'NOTE_UPDATE', data: {id: noteId, name: noteName, config : noteConfig}});
//  }

  moveParagraph(noteId: string, paragraphId: string, newIndex: number) {
    this.notebookServiceImpl.moveParagraph(noteId, paragraphId, newIndex);
  }

  insertParagraph(noteId: string, newIndex: number) {
    this.notebookServiceImpl.insertParagraph(noteId, newIndex);
  }

//  TODO What is this?
//  updateAngularObject: function(noteId, name, value, interpreterGroupId) {
//  this.sendJSON({
//        op: 'ANGULAR_OBJECT_UPDATED',
//        data: {
//          noteId: noteId,
//          name: name,
//          value: value,
//          interpreterGroupId: interpreterGroupId
//        }
//      });
//  }

//  cancelParagraphRun(paragraphId: string) {
//    this.sendJSON({op: 'CANCEL_PARAGRAPH', data: {id: paragraphId}});
//  }

//  TODO Types for the remaining params.
//  runParagraph(paragraphId: string, paragraphTitle: string, paragraphData, paragraphConfig, paragraphParams) {
//    this.sendJSON({
//      op: 'RUN_PARAGRAPH',
//      data: {
//        id: paragraphId,
//        title: paragraphTitle,
//        paragraph: paragraphData,
//        config: paragraphConfig,
//        params: paragraphParams
//      }
//    });
//  }

  removeParagraph(noteId: string, paragraphId: string) {
    this.notebookServiceImpl.removeParagraph(noteId, paragraphId);
  }

//  clearParagraphOutput(paragraphId: string) {
//    this.sendJSON({op: 'PARAGRAPH_CLEAR_OUTPUT', data: {id: paragraphId}});
//  }

//  completion: function(paragraphId, buf, cursor) {
//      websocketEvents.sendNewEvent({
//        op : 'COMPLETION',
//        data : {
//          id : paragraphId,
//          buf : buf,
//          cursor : cursor
//        }
//      });
//    }

  // TODO Consider commitParagraph instead of this?
  saveParagraph(noteId: string, paragraphId: string) {
    this.notebookServiceImpl.saveParagraph(noteId, paragraphId);
  }

//    commitParagraph: function(paragraphId, paragraphTitle, paragraphData, paragraphConfig, paragraphParams) {
//      websocketEvents.sendNewEvent({
//        op: 'COMMIT_PARAGRAPH',
//        data: {
//          id: paragraphId,
//          title : paragraphTitle,
//          paragraph: paragraphData,
//          config: paragraphConfig,
//          params: paragraphParams
//        }
//      });
//    }

  // TODO type
//  importNotebook(notebook) {
//    this.sendJSON({
//        op: 'IMPORT_NOTE',
//        data: {
//          notebook: notebook
//        }
//      });
//    }

  isConnected(): boolean {
    return this.notebookServiceImpl.isConnected();
  }
}
