import {autoinject} from 'aurelia-framework';
import {EventAggregator, Subscription} from 'aurelia-event-aggregator';
import {NotebookService} from './notebook-service';
import {NoteM} from './note-m';
import {Router} from 'aurelia-router';

@autoinject
export class Notebook {

  eventAggregator: EventAggregator;

  notebookService: NotebookService;

  router: Router;

  id: string;

  note: NoteM;

  connectionStatusSubscription: Subscription;

  getNoteSubscription: Subscription;

  isBound: boolean = false;

  isActivated: boolean = false;

  isFetched: boolean = false;

  constructor(eventAggregator: EventAggregator, notebookService: NotebookService, router: Router) {
    this.eventAggregator = eventAggregator;
    this.notebookService = notebookService;
    this.router = router;
  }

  bind() {
    this.connectionStatusSubscription = this.eventAggregator.subscribe('connectionstatus', payload => {
      if (payload === 'open') {
        if (this.isActivated && this.isBound) {
          this.fetch();
        }
      }
    });
    this.getNoteSubscription = this.eventAggregator.subscribe('getNote', payload => {
      if (!payload) {
        // TODO Show error and navigate to error page
        this.router.navigate('welcome');
      }
      if (payload.id == this.id) {
        this.note = payload;
      }
    });
    if (this.isActivated && this.notebookService.isConnected()) {
      this.fetch();
    }
    this.isBound = true;
  }

  attached() {
  }

  detached() {
  }

  unbind() {
    this.connectionStatusSubscription.dispose();
    this.getNoteSubscription.dispose();
    this.isBound = false;
  }

  activate(params) {
    // TODO Redirect to an error page if there is no params.id.
    this.id = params.id;
    // Navigation and component lifecycles are not nested in a unique way.
    // This component is attached once, but activated before the attachement
    // if not yet attached, and activated but not attached again if it is
    // already attached. Hence we check for being attached for fetching the note.
    if (this.isBound && this.notebookService.isConnected()) {
      this.fetch();
    }
    this.isActivated = true;
  }

  deactivate() {
    this.isFetched = false;
    this.isActivated = false;
  }

  fetch() {
    if (!this.isFetched) {
      this.notebookService.getNote(this.id);
      this.isFetched = true;
    }
  }

  insertParagraph(index) {
    this.notebookService.insertParagraph(this.note.id, index);
  }
}
