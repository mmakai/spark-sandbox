import {EventAggregator} from 'aurelia-event-aggregator';
import * as LogManager from 'aurelia-logging';
import {NoteInfoM} from './note-info-m';
import {NoteM} from './note-m';

export class NotebookServiceWebSocketImpl {

  logger = LogManager.getLogger('NotebookServiceWebsocketImpl');

  eventAggregator: EventAggregator;

  noteInfos: NoteInfoM[] = [];

  // TODO This should be a map.
  notes: NoteM[] = [];

  url: string = 'ws://localhost:9200/notes';

  socket: WebSocket;

  constructor(eventAggregator: EventAggregator) {
    this.eventAggregator = eventAggregator;
  }

  connect() {
    if (this.socket) {
      this.socket.close();
      this.socket.onclose = null;
      this.socket.onerror = null;
      this.socket.onmessage = null;
      this.socket.onopen = null;
    }
    this.socket = new WebSocket(this.url);
    this.socket.onclose = e => {
      this.logger.info(`Connection to ${this.url} closed.`);
      this.eventAggregator.publish('connectionstatus', 'close');
      this.noteInfos.splice(0, this.noteInfos.length);
    };
    this.socket.onerror = e => {
      this.logger.error(JSON.stringify(e));
    };
    this.socket.onmessage = e => {
      let payload = JSON.parse(e.data);
      let op = payload.op;
      let data = payload.data;
      if (op === 'GET_NOTE_INFOS') {
      } else if (op === 'NOTE_INFOS') {
        this.noteInfos.splice(0, this.noteInfos.length);
        for (let noteInfo of data.noteInfos) {
          this.noteInfos.push(noteInfo);
        }
      } else if (op === 'CREATE_NOTE') {
      } else if (op === 'GET_NOTE') {
      } else if (op === 'NOTE') {
        let noteId = data.note.id;
        this.notes[noteId] = data.note;
        this.eventAggregator.publish('getNote', this.notes[noteId]);
      } else if (op === 'INSERT_PARAGRAPH') {
      } else if (op === 'NEW_PARAGRAPH') {
        let noteId = data.noteId;
        let index = data.index;
        if (!this.notes[noteId].paragraphs) {
          this.notes[noteId].paragraphs = [];
        }
        this.notes[noteId].paragraphs.splice(index, 0, data.paragraph);
      } else if (op === 'MOVE_PARAGRAPH') {
      } else if (op === 'PARAGRAPH_MOVED') {
        let noteId = data.noteId;
        let paragraphs = this.notes[noteId].paragraphs;
        let paragraphId = data.id;
        let index = paragraphs.findIndex(p => p.id == paragraphId);
        let newIndex = data.index;
        if (index >= 0 && index != newIndex) {
          // NOTE Simple swap of paragraphs does not work, as repeat.for is not
          // observing that. Hence we first delete a paragraph and insert it
          // at the new index.
          let paragraph = paragraphs.splice(index, 1);
          paragraphs.splice(newIndex, 0, paragraph[0]);
        }
      } else if (op === 'REMOVE_PARAGRAPH') {
      } else if (op === 'PARAGRAPH_REMOVED') {
        let noteId = data.noteId;
        let paragraphs = this.notes[noteId].paragraphs;
        let paragraphId = data.id;
        let index = paragraphs.findIndex(p => p.id == paragraphId);
        if (index >= 0 && index < paragraphs.length) {
          paragraphs.splice(index, 1);
        }
      } else if (op === 'SAVE_PARAGRAPH') {
      } else if (op === 'PARAGRAPH_SAVED') {
        let noteId = data.noteId;
        let paragraphs = this.notes[noteId].paragraphs;
        let paragraphId = data.id;
        let index = paragraphs.findIndex(p => p.id == paragraphId);
        if (index >= 0 && index < paragraphs.length) {
          paragraphs[index].text = data.text;
          paragraphs[index].editorText = data.text;
        }
      }
    };
    this.socket.onopen = e => {
      this.logger.info(`Connection to ${this.url} opened.`);
      this.eventAggregator.publish('connectionstatus', 'open');
      this.eventAggregator.publish('getNoteInfos', this.noteInfos);
      this.getNoteInfos();
    };
  }

  close() {
    if (this.socket) {
      this.socket.close();
    }
  }

  sendJSON(json) {
    this.logger.info(json.op);
    this.socket.send(JSON.stringify(json));
  }

  getHomeNote() {
  }

  createNote(noteName: string) {
    this.sendJSON({op: 'CREATE_NOTE', data: {name: noteName}});
  }

  deleteNote(noteId: string) {
  }

  cloneNote(noteIdToClone: string, newNoteName: string) {
  }

  getNoteInfos() {
    this.sendJSON({op: 'GET_NOTE_INFOS'});
  }

  getNote(noteId: string) {
    this.sendJSON({op: 'GET_NOTE', data: {id: noteId}});
  }

  moveParagraph(noteId: string, paragraphId: string, newIndex: number) {
    this.sendJSON({op: 'MOVE_PARAGRAPH', data: {noteId: noteId, id: paragraphId, index: newIndex}});
  }

  insertParagraph(noteId: string, newIndex: number) {
    this.sendJSON({op: 'INSERT_PARAGRAPH', data: {noteId: noteId, index: newIndex}});
  }

  saveParagraph(noteId: string, paragraphId: string) {
    let paragraphs = this.notes[noteId].paragraphs;
    let index = paragraphs.findIndex(p => p.id == paragraphId);
    this.sendJSON({op: 'SAVE_PARAGRAPH', data: {noteId: noteId, id: paragraphId, text: paragraphs[index].editorText}});
  }

  removeParagraph(noteId: string, paragraphId: string) {
    this.sendJSON({op: 'REMOVE_PARAGRAPH', data: {noteId: noteId, id: paragraphId}});
  }

  isConnected() {
    return this.socket ? this.socket.readyState == WebSocket.OPEN : false;
  }
}
