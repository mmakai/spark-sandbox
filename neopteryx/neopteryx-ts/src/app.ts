import {autoinject} from 'aurelia-framework';
import {NotebookService} from './notebook-service';
import {Router, RouterConfiguration} from 'aurelia-router'

@autoinject
export class App {

  notebookService: NotebookService;

  router: Router;

  constructor(notebookService: NotebookService) {
    this.notebookService = notebookService;
  }
  
  configureRouter(config: RouterConfiguration, router: Router) {
    config.title = 'Neopteryx';
    config.map([
      { route: ['', 'welcome'], name: 'welcome',      moduleId: 'welcome',      nav: true, title: 'Welcome' },
      { route: 'notebook/:id',  name: 'notebook',     moduleId: 'notebook',     nav: false, title: 'Notebook' },
      { route: 'users',         name: 'users',        moduleId: 'users',        nav: true, title: 'Github Users' },
      { route: 'child-router',  name: 'child-router', moduleId: 'child-router', nav: true, title: 'Child Router' }
    ]);
    this.router = router;
  }
}
