package org.neopteryx;

import java.util.HashMap;
import java.util.Map;

public class Message {

  public enum Op {

    GET_NOTE_INFOS, // [c-s] ask list of note infos

    NOTE_INFOS, // [s-c] list of note infos
    // @param notes serialized List<NoteInfo> object

    CREATE_NOTE, // [c-s] create new note

    GET_NOTE, // [c-s] client load note
    // @param id note id

    NOTE, // [s-c] note info
    // @param note serlialized Note object

    INSERT_PARAGRAPH, // [c-s] create new paragraph below current paragraph
    // @param note id
    // @param target index

    NEW_PARAGRAPH, // [s-c] paragraph info
    // @param note id
    // @param paragraph index
    // @param paragraph serialized paragraph object

    MOVE_PARAGRAPH, // [c-s] move paragraph order
    // @param note id
    // @param paragraph id
    // @param index index the paragraph want to go

    PARAGRAPH_MOVED, // [s-c]
    // @param note id
    // @param paragraph index

    SAVE_PARAGRAPH, // [c-s]
    // @param note id
    // paragraph text

    PARAGRAPH_SAVED,
    // @param note id
    // @param paragraph id
    // @param text

    REMOVE_PARAGRAPH, // [c-s]
    // @param note id
    // @param paragraph id

    PARAGRAPH_REMOVED, // [s-c]
    // @param note id
    // @param paragraph id
  }

  public Op op;

  public Map<String, Object> data = new HashMap<>();

  public Message(Op op) {
    this.op = op;
  }

  public Message put(String k, Object v) {
    data.put(k, v);
    return this;
  }

  public Object get(String k) {
    return data.get(k);
  }

  @Override
  public String toString() {
    return "Message [op=" + op + ", data=" + data + "]";
  }
}
