package org.neopteryx;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.neopteryx.repo.NoteInfo;

public class MockNotebook implements Notebook {

  private Map<String, Note> notes = new HashMap<>();

  public MockNotebook() {
    Note note = new Note();
    note.setName("apple 5");
    Paragraph p = note.addParagraph();
    p.setText("orange 6");
    notes.put(note.getId(), note);
  }

  @Override
  public List<NoteInfo> getNoteInfos() {
    List<NoteInfo> noteInfos = new ArrayList<>();
    synchronized (notes) {
      for (Note note : notes.values()) {
        NoteInfo noteInfo = new NoteInfo(note.getId(), note.getName());
        noteInfos.add(noteInfo);
      } ;
    }
    noteInfos.sort(NOTE_INFO_COMPARATOR);
    return noteInfos;
  }

  @Override
  public Note createNote() {
    Note note = new Note();
    synchronized (notes) {
      notes.put(note.getId(), note);
    }
    return note;
  }

  @Override
  public Note getNote(String id) {
    synchronized (notes) {
      return notes.get(id);
    }
  }

  @Override
  public void persist(Note note) {}
}
