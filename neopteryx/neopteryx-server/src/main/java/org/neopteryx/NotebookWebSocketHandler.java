package org.neopteryx;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketClose;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketConnect;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketError;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketMessage;
import org.eclipse.jetty.websocket.api.annotations.WebSocket;
import org.neopteryx.Message.Op;
import org.neopteryx.repo.NoteInfo;
import org.neopteryx.repo.NotebookRepo;
import org.neopteryx.repo.VFSNotebookRepo;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@WebSocket
public class NotebookWebSocketHandler {

  private static Logger logger = LogManager.getLogger(NotebookWebSocketHandler.class);

  private Gson gson = new GsonBuilder().create();
  // .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ").create();

  private NeopteryxConfig config;

  private Collection<Session> sessions;

  private NotebookRepo repo;

  private Notebook notebook;

  public NotebookWebSocketHandler(NeopteryxConfig config, Collection<Session> sessions) {
    this.config = config;
    this.sessions = sessions;
    try {
      repo = new VFSNotebookRepo(config);
      notebook = new TheNotebook(repo);
    } catch (IOException e) {
      logger.error(e);
    }
  }

  private static String shortInfo(Session session) {
    return session.getRemoteAddress().getAddress().toString() + ':'
        + session.getRemoteAddress().getPort();
  }

  @OnWebSocketConnect
  public void onConnect(Session user) throws Exception {
    List<String> info = sessions.stream().map(s -> shortInfo(s)).collect(Collectors.toList());
    logger.info("Sessions: " + info);
    logger.info("Adding session: " + shortInfo(user));
    sessions.add(user);
    // broadcastMessage(user, "joined");
  }

  @OnWebSocketClose
  public void onClose(Session user, int statusCode, String reason) {
    List<String> info = sessions.stream().map(s -> shortInfo(s)).collect(Collectors.toList());
    logger.info("Sessions: " + info);
    logger.info("Removing session: " + shortInfo(user));
    sessions.remove(user);
    // broadcastMessage(user, "left");
  }

  @OnWebSocketMessage
  public void onMessage(Session user, String message) {
    List<String> info = sessions.stream().map(s -> shortInfo(s)).collect(Collectors.toList());
    logger.info("Sessions: " + info);
    logger.info("Message: " + shortInfo(user) + " " + message);
    try {
      Message messageReceived = deserializeMessage(message);
      switch (messageReceived.op) {
        case GET_NOTE_INFOS:
          unicastNoteInfos(user);
          break;
        case NOTE_INFOS:
          break;
        case CREATE_NOTE:
          createNote(user, messageReceived);
          break;
        case GET_NOTE:
          sendNote(user, messageReceived);
          break;
        case NOTE:
          break;
        case INSERT_PARAGRAPH:
          insertParagraph(user, messageReceived);
          break;
        case NEW_PARAGRAPH:
          break;
        case MOVE_PARAGRAPH:
          moveParagraph(user, messageReceived);
          break;
        case PARAGRAPH_MOVED:
          break;
        case SAVE_PARAGRAPH:
          saveParagraph(user, messageReceived);
          break;
        case PARAGRAPH_SAVED:
          break;
        case REMOVE_PARAGRAPH:
          removeParagraph(user, messageReceived);
          break;
        case PARAGRAPH_REMOVED:
          break;
        default:
          throw new UnsupportedOperationException();
      }
    } catch (IOException e) {
      logger.error(e);
    }
  }

  @OnWebSocketError
  public void onError(Session user, Throwable e) {
    List<String> info = sessions.stream().map(s -> shortInfo(s)).collect(Collectors.toList());
    logger.info("Sessions: " + info);
    logger.error(shortInfo(user), e);
  }

  protected Message deserializeMessage(String msg) {
    return gson.fromJson(msg, Message.class);
  }

  protected String serializeMessage(Message m) {
    return gson.toJson(m);
  }

  // public void broadcastMessage(Session user, String message) {
  // sessions.stream().filter(Session::isOpen).forEach(session -> {
  // try {
  // session.getRemote().sendString(shortInfo(user) + " " + message);
  // } catch (IOException e) {
  // List<String> info = sessions.stream()
  // .map(s -> shortInfo(s)).collect(Collectors.toList());
  // logger.info("Sessions: " + info);
  // logger.error(shortInfo(user), e);
  // }
  // });
  // }

  private void broadcast(Message m) {
    for (Session user : sessions) {
      try {
        user.getRemote().sendString(serializeMessage(m));
      } catch (IOException e) {
        logger.error("Socket error: ", e);
      }
    }
  }

  // private void broadcast(String noteId, Message m) {
  // synchronized (noteSocketMap) {
  // List<NotebookSocket> socketLists = noteSocketMap.get(noteId);
  // if (socketLists == null || socketLists.size() == 0) {
  // return;
  // }
  // LOG.debug("SEND >> " + m.op);
  // for (NotebookSocket conn : socketLists) {
  // try {
  // conn.send(serializeMessage(m));
  // } catch (IOException e) {
  // LOG.error("socket error", e);
  // }
  // }
  // }
  // }

  private void unicast(Session user, Message m) {
    try {
      user.getRemote().sendString(serializeMessage(m));
    } catch (IOException e) {
      logger.error("Socket error: ", e);
    }
  }

  // public void broadcastNote(Note note) {
  // broadcast(note.getId(), new Message(OP.NOTE).put("note", note));
  // }

  public void broadcastNoteInfos() {
    List<NoteInfo> noteInfos = notebook.getNoteInfos();
    broadcast(new Message(Op.NOTE_INFOS).put("noteInfos", noteInfos));
  }

  public void unicastNoteInfos(Session user) {
    List<NoteInfo> noteInfos = notebook.getNoteInfos();
    unicast(user, new Message(Op.NOTE_INFOS).put("noteInfos", noteInfos));
  }

  public void broadcastParagraph(String noteId, int index, Paragraph p) {
    // TODO Send only to clients working on this note.
    broadcast(new Message(Op.NEW_PARAGRAPH).put("noteId", noteId).put("index", index)
        .put("paragraph", p));
  }

  public void broadcastParagraphMoved(String noteId, String id, int index) {
    // TODO Send only to clients working on this note.
    broadcast(
        new Message(Op.PARAGRAPH_MOVED).put("noteId", noteId).put("id", id).put("index", index));
  }

  public void broadcastParagraphSaved(String noteId, String id, String text) {
    // TODO Send only to clients working on this note.
    broadcast(
        new Message(Op.PARAGRAPH_SAVED).put("noteId", noteId).put("id", id).put("text", text));
  }

  public void broadcastParagraphRemoved(String noteId, String id) {
    // TODO Send only to clients working on this note.
    broadcast(new Message(Op.PARAGRAPH_REMOVED).put("noteId", noteId).put("id", id));
  }

  private void createNote(Session user, Message message) throws IOException {
    Note note = notebook.createNote();
    if (message != null) {
      String name = (String) message.get("name");
      note.setName(name);
    }
    notebook.persist(note);
    broadcastNoteInfos();
  }

  private void sendNote(Session user, Message fromMessage) throws IOException {
    String noteId = (String) fromMessage.get("id");
    if (noteId == null) {
      return;
    }
    Note note = notebook.getNote(noteId);
    if (note != null) {
      unicast(user, new Message(Op.NOTE).put("note", note));
    }
  }

  private void insertParagraph(Session user, Message fromMessage) throws IOException {
    String noteId = (String) fromMessage.get("noteId");
    final int index = (int) Double.parseDouble(fromMessage.get("index").toString());
    final Note note = notebook.getNote(noteId);
    Paragraph p = note.insertParagraph(index);
    notebook.persist(note);
    broadcastParagraph(noteId, index, p);
  }

  private void moveParagraph(Session user, Message fromMessage) throws IOException {
    String noteId = (String) fromMessage.get("noteId");
    final String id = (String) fromMessage.get("id");
    if (id == null) {
      return;
    }
    final int newIndex = (int) Double.parseDouble(fromMessage.get("index").toString());
    final Note note = notebook.getNote(noteId);
    note.moveParagraph(id, newIndex);
    notebook.persist(note);
    broadcastParagraphMoved(noteId, id, newIndex);
  }

  private void saveParagraph(Session user, Message fromMessage) throws IOException {
    String noteId = (String) fromMessage.get("noteId");
    final String id = (String) fromMessage.get("id");
    final String text = (String) fromMessage.get("text");
    if (id == null) {
      return;
    }
    final Note note = notebook.getNote(noteId);
    note.getParagraph(id).setText(text);
    notebook.persist(note);
    broadcastParagraphSaved(noteId, id, text);
  }

  private void removeParagraph(Session user, Message fromMessage) throws IOException {
    String noteId = (String) fromMessage.get("noteId");
    final String id = (String) fromMessage.get("id");
    if (id == null) {
      return;
    }
    final Note note = notebook.getNote(noteId);
    note.removeParagraph(id);
    notebook.persist(note);
    broadcastParagraphRemoved(noteId, id);
  }
}
