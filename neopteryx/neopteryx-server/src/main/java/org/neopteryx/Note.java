package org.neopteryx;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import org.apache.zeppelin.notebook.utility.IdHashes;

public class Note {

  private List<Paragraph> paragraphs;

  private String id;

  private String name;

  // private ZonedDateTime dateUpdated;

  public Note() {
    id = generateId();
  }

  public Note(String id, String name, List<Paragraph> paragraphs) {
    this.id = id;
    this.name = name;
    this.paragraphs = paragraphs;
  }

  private static String generateId() {
    return IdHashes.encode(System.currentTimeMillis() + new Random().nextInt());
  }

  public String getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Paragraph addParagraph() {
    synchronized (this) {
      if (paragraphs == null) {
        paragraphs = new ArrayList<>();
      }
    }
    Paragraph p = new Paragraph();
    synchronized (paragraphs) {
      paragraphs.add(p);
    }
    return p;
  }

  public Paragraph insertParagraph(int index) {
    synchronized (this) {
      if (paragraphs == null) {
        paragraphs = new ArrayList<>();
      }
    }
    Paragraph p = new Paragraph();
    synchronized (paragraphs) {
      paragraphs.add(index, p);
    }
    return p;
  }

  public void moveParagraph(String id, int newIndex) {
    this.moveParagraph(id, newIndex, true);
  }

  public void moveParagraph(String id, int index, boolean throwWhenIndexIsOutOfBound) {
    synchronized (paragraphs) {
      int oldIndex;
      Paragraph p = null;
      if (index < 0 || index >= paragraphs.size()) {
        if (throwWhenIndexIsOutOfBound) {
          throw new IndexOutOfBoundsException(
              "paragraph size is " + paragraphs.size() + " , index is " + index);
        } else {
          return;
        }
      }
      for (int i = 0; i < paragraphs.size(); i++) {
        if (paragraphs.get(i).getId().equals(id)) {
          oldIndex = i;
          if (oldIndex == index) {
            return;
          }
          p = paragraphs.remove(i);
        }
      }
      if (p != null) {
        paragraphs.add(index, p);
      }
    }
  }

  public Paragraph removeParagraph(String id) {
    synchronized (paragraphs) {
      Iterator<Paragraph> i = paragraphs.iterator();
      while (i.hasNext()) {
        Paragraph p = i.next();
        if (p.getId().equals(id)) {
          i.remove();
          return p;
        }
      }
    }
    return null;
  }

  public Paragraph getParagraph(String id) {
    for (Paragraph p : paragraphs) {
      if (p.getId().equals(id)) {
        return p;
      }
    }
    return null;
  }

  public List<Paragraph> getParagraphs() {
    return paragraphs;
  }
}
