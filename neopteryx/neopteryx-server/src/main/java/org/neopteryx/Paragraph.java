package org.neopteryx;

import java.util.Random;

public class Paragraph {

  private String id;

  private String name;

  private String text;

  // private ZonedDateTime dateUpdated;

  public Paragraph() {
    id = generateId();
  }

  public Paragraph(String id, String name, String text) {
    this.id = id;
    this.name = name;
    this.text = text;
  }

  private static String generateId() {
    return System.currentTimeMillis() + "_" + new Random(System.currentTimeMillis()).nextInt();
  }

  public String getId() {
    return id;
  }

  // public void setId(String id) {
  // this.id = id;
  // }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }
}
