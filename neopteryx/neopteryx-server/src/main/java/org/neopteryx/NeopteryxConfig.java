package org.neopteryx;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

import org.apache.commons.configuration2.XMLConfiguration;
import org.apache.commons.configuration2.builder.fluent.Configurations;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class NeopteryxConfig {

  private static Logger logger = LogManager.getLogger(NeopteryxConfig.class);

  private static final String CONFIG_XML = "conf/conf.xml";

  private static XMLConfiguration xmlConfig;

  private static NeopteryxConfig neopteryxConfig;

  public NeopteryxConfig() {}

  public NeopteryxConfig(URL url) throws ConfigurationException {
    Configurations configs = new Configurations();
    xmlConfig = configs.xml(url);
  }

  public static synchronized NeopteryxConfig create() {
    if (neopteryxConfig != null) {
      return neopteryxConfig;
    }
    ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
    URL url = null;
    // Load conf/conf.xml from the directory of the jar.
    File jarPath = new File(
        NeopteryxConfig.class.getProtectionDomain().getCodeSource().getLocation().getPath());
    try {
      File f = new File(jarPath.getParentFile(), CONFIG_XML);
      if (f.exists()) {
        url = f.toURI().toURL();
      }
    } catch (MalformedURLException e) {
      logger.error(e);
    }
    if (url == null) {
      // Load conf/conf.xml from the resoureces inside the jar.
      ClassLoader cl = NeopteryxConfig.class.getClassLoader();
      if (cl != null) {
        url = cl.getResource(CONFIG_XML);
      }
    }
    if (url == null) {
      url = classLoader.getResource(CONFIG_XML);
    }
    if (url == null) {
      logger.warn("Failed to load configuration, proceeding with a default");
      neopteryxConfig = new NeopteryxConfig();
    } else {
      try {
        logger.info("Load configuration from " + url);
        neopteryxConfig = new NeopteryxConfig(url);
      } catch (ConfigurationException e) {
        logger.warn("Failed to load configuration from " + url + " proceeding with a default", e);
        neopteryxConfig = new NeopteryxConfig();
      }
    }
    return neopteryxConfig;
  }

  public enum Var {

    HOME("home", "."),
    PORT("server.port", 9200),
    ENCODING("encoding", "UTF-8"),
    NOTEBOOK_DIR("notebook.dir", "notebook");

    private String name;

    private String stringValue;

    private int intValue;

    private Var(String name, String stringValue) {
      this.name = name;
      this.stringValue = stringValue;
    }

    private Var(String name, int intValue) {
      this.name = name;
      this.intValue = intValue;
    }
  }

  public String getRelativeDir(String path) {
    if (path != null && path.startsWith("/") || isWindowsPath(path)) {
      return path;
    } else {
      return getString(Var.HOME) + "/" + path;
    }
  }

  public boolean isWindowsPath(String path) {
    return path.matches("^[A-Za-z]:\\\\.*");
  }

  public String getNotebookDir() {
    return getString(Var.NOTEBOOK_DIR);
  }

  // TODO Read environment variables as well.
  public String getString(Var var) {
    if (xmlConfig != null) {
      return xmlConfig.getString(var.name, var.stringValue);
    } else {
      return var.stringValue;
    }
  }

  // TODO Read environment variables as well.
  public Integer getInteger(Var var) {
    if (xmlConfig != null) {
      return xmlConfig.getInteger(var.name, var.intValue);
    } else {
      return var.intValue;
    }
  }
}
