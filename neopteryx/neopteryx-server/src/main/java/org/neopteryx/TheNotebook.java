package org.neopteryx;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.neopteryx.repo.NoteInfo;
import org.neopteryx.repo.NotebookRepo;

public class TheNotebook implements Notebook {

  private static Logger logger = LogManager.getLogger(TheNotebook.class);

  private Map<String, String> noteInfos;

  private Map<String, Note> notes = new HashMap<>();

  private NotebookRepo repo;

  public TheNotebook(NotebookRepo repo) {
    this.repo = repo;
  }

  @Override
  public synchronized List<NoteInfo> getNoteInfos() {
    if (noteInfos == null) {
      try {
        noteInfos = new HashMap<>();
        for (NoteInfo noteInfo : repo.list()) {
          noteInfos.put(noteInfo.getId(), noteInfo.getName());
        }
      } catch (IOException e) {
        logger.error(e);
      }
    }
    for (Entry<String, Note> e : notes.entrySet()) {
      noteInfos.put(e.getKey(), e.getValue().getName());
    }
    List<NoteInfo> result = new ArrayList<>();
    for (Entry<String, String> e : noteInfos.entrySet()) {
      result.add(new NoteInfo(e.getKey(), e.getValue()));
    }
    result.sort(NOTE_INFO_COMPARATOR);
    return result;
  }

  @Override
  public synchronized Note createNote() {
    Note note = new Note();
    notes.put(note.getId(), note);
    return note;
  }

  @Override
  public synchronized Note getNote(String id) {
    Note note = notes.get(id);
    if (note == null) {
      try {
        org.neopteryx.repo.Note n = repo.get(id);
        List<Paragraph> pp = null;
        if (n.getParagraphs() != null) {
          pp = new ArrayList<>();
          for (org.neopteryx.repo.Paragraph p : n.getParagraphs()) {
            pp.add(new Paragraph(p.getId(), p.getName(), p.getText()));
          }
        }
        note = new Note(n.getId(), n.getName(), pp);
        notes.put(id, note);
      } catch (IOException e) {
        logger.error(e);
      }
    }
    return note;
  }

  @Override
  public void persist(Note note) {
    try {
      List<org.neopteryx.repo.Paragraph> pp = null;
      if (note.getParagraphs() != null) {
        pp = new ArrayList<>();
        for (Paragraph p : note.getParagraphs()) {
          pp.add(new org.neopteryx.repo.Paragraph(p.getId(), p.getName(), p.getText()));
        }
      }
      org.neopteryx.repo.Note n = new org.neopteryx.repo.Note(note.getId(), note.getName(), pp);
      repo.save(n);
    } catch (IOException e) {
      logger.error(e);
    }
  }
}
