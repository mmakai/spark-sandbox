package org.neopteryx;

import java.io.IOException;
import java.net.URL;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.AbstractHandler;
import org.eclipse.jetty.server.handler.ContextHandler;
import org.eclipse.jetty.server.handler.ContextHandlerCollection;
import org.eclipse.jetty.server.handler.ResourceHandler;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.util.resource.Resource;
import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.servlet.ServletUpgradeRequest;
import org.eclipse.jetty.websocket.servlet.ServletUpgradeResponse;
import org.eclipse.jetty.websocket.servlet.WebSocketCreator;
import org.eclipse.jetty.websocket.servlet.WebSocketServlet;
import org.eclipse.jetty.websocket.servlet.WebSocketServletFactory;

public class NeopteryxServer {

  public static class Handler extends AbstractHandler {

    private String hello;

    public Handler(String hello) {
      this.hello = hello;
    }

    @Override
    public void handle(String target, Request baseRequest, HttpServletRequest request,
        HttpServletResponse response) throws IOException, ServletException {
      response.setContentType("text/html; charset=utf-8");
      response.setStatus(HttpServletResponse.SC_OK);
      response.getWriter().println("<h1>" + hello + "</h1>");
      baseRequest.setHandled(true);
    }
  }

  @SuppressWarnings("serial")
  public static class NotebookWebSocketServlet extends WebSocketServlet {

    private NeopteryxConfig config;

    public class NotebookWebSocketCreator implements WebSocketCreator {

      private Queue<Session> sessions = new ConcurrentLinkedQueue<>();

      private NotebookWebSocketHandler handler = new NotebookWebSocketHandler(config, sessions);

      @Override
      public Object createWebSocket(ServletUpgradeRequest req, ServletUpgradeResponse resp) {
        return handler;
      }
    }

    public NotebookWebSocketServlet(NeopteryxConfig config) {
      this.config = config;
    }

    @Override
    public void configure(WebSocketServletFactory factory) {
      // factory.register(NotebookWebSocketHandler.class);
      factory.setCreator(new NotebookWebSocketCreator());
      // half hour.
      factory.getPolicy().setIdleTimeout(1800 * 1000);
    }
  }

  public static void main(String[] args) throws Exception {
    NeopteryxConfig config = NeopteryxConfig.create();

    ContextHandlerCollection handlers = new ContextHandlerCollection();

    ContextHandler context1 = new ContextHandler("/hello");
    context1.setHandler(new Handler("Hello"));
    handlers.addHandler(context1);

    ContextHandler context2 = new ContextHandler("/fr");
    context2.setHandler(new Handler("Salut"));
    handlers.addHandler(context2);

    URL webDir = NeopteryxServer.class.getClassLoader().getResource("static");
    // String webDir = "/home/marci/spark-sandbox-2/neopteryx/neopteryx-ts/";

    ContextHandler context3 = new ContextHandler();
    context3.setContextPath("/");
    ResourceHandler resourceHandler = new ResourceHandler();
    resourceHandler.setBaseResource(Resource.newResource(webDir));
    context3.setHandler(resourceHandler);
    handlers.addHandler(context3);

    ServletContextHandler context4 = new ServletContextHandler();
    context4.setContextPath("/");
    ServletHolder servletHolder = new ServletHolder(new NotebookWebSocketServlet(config));
    context4.addServlet(servletHolder, "/notes");
    handlers.addHandler(context4);

    Server server = new Server(config.getInteger(NeopteryxConfig.Var.PORT));
    server.setHandler(handlers);
    server.start();
    server.join();
  }
}
