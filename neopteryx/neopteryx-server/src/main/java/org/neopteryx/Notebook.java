package org.neopteryx;

import java.util.Comparator;
import java.util.List;

import org.neopteryx.repo.NoteInfo;

public interface Notebook {

  Comparator<NoteInfo> NOTE_INFO_COMPARATOR = new Comparator<NoteInfo>() {

    @Override
    public int compare(NoteInfo noteInfo1, NoteInfo noteInfo2) {
      String name1 = noteInfo1.getId();
      if (noteInfo1.getName() != null) {
        name1 = noteInfo1.getName();
      }
      String name2 = noteInfo2.getId();
      if (noteInfo2.getName() != null) {
        name2 = noteInfo2.getName();
      }
      return name1.compareTo(name2);
    }
  };

  List<NoteInfo> getNoteInfos();

  Note createNote();

  Note getNote(String id);

  void persist(Note note);
}
