package org.neopteryx.repo;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.commons.vfs2.FileContent;
import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileSystemManager;
import org.apache.commons.vfs2.FileType;
import org.apache.commons.vfs2.NameScope;
import org.apache.commons.vfs2.VFS;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.neopteryx.NeopteryxConfig;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class VFSNotebookRepo implements NotebookRepo {

  private static Logger logger = LogManager.getLogger(VFSNotebookRepo.class);

  private FileSystemManager fsManager;

  private URI filesystemRoot;

  private NeopteryxConfig config;

  public VFSNotebookRepo(NeopteryxConfig config) throws IOException {
    this.config = config;
    try {
      if (config.isWindowsPath(config.getNotebookDir())) {
        filesystemRoot = new File(config.getNotebookDir()).toURI();
      } else {
        filesystemRoot = new URI(config.getNotebookDir());
      }
    } catch (URISyntaxException e) {
      throw new IOException(e);
    }
    if (filesystemRoot.getScheme() == null) { // it is local path
      try {
        filesystemRoot =
            new URI(new File(config.getRelativeDir(filesystemRoot.getPath())).getAbsolutePath());
      } catch (URISyntaxException e) {
        throw new IOException(e);
      }
    }
    fsManager = VFS.getManager();
    FileObject file = fsManager.resolveFile(filesystemRoot.getPath());
    if (!file.exists()) {
      logger.info("Notebook dir doesn't exist, create.");
      file.createFolder();
    }
  }

  private String getPath(String path) {
    if (path == null || path.trim().length() == 0) {
      return filesystemRoot.toString();
    }
    if (path.startsWith("/")) {
      return filesystemRoot.toString() + path;
    } else {
      return filesystemRoot.toString() + "/" + path;
    }
  }

  private boolean isDirectory(FileObject fo) throws IOException {
    if (fo == null)
      return false;
    if (fo.getType() == FileType.FOLDER) {
      return true;
    } else {
      return false;
    }
  }

  @Override
  public List<NoteInfo> list() throws IOException {
    FileObject rootDir = getRootDir();
    FileObject[] children = rootDir.getChildren();
    List<NoteInfo> infos = new ArrayList<>();
    for (FileObject f : children) {
      String fileName = f.getName().getBaseName();
      if (f.isHidden()
          || fileName.startsWith(".")
          || fileName.startsWith("#")
          || fileName.startsWith("~")) {
        // skip hidden, temporary files
        continue;
      }
      if (!isDirectory(f)) {
        // currently single note is saved like, [NOTE_ID]/note.json.
        // so it must be a directory
        continue;
      }
      NoteInfo info = null;
      try {
        info = getNoteInfo(f);
        if (info != null) {
          infos.add(info);
        }
      } catch (Exception e) {
        logger.error("Can't read note " + f.getName().toString(), e);
      }
    }
    return infos;
  }

  private Note getNote(FileObject noteDir) throws IOException {
    if (!isDirectory(noteDir)) {
      throw new IOException(noteDir.getName().toString() + " is not a directory");
    }
    FileObject noteJson = noteDir.resolveFile("note.json", NameScope.CHILD);
    if (!noteJson.exists()) {
      throw new IOException(noteJson.getName().toString() + " not found");
    }
    GsonBuilder gsonBuilder = new GsonBuilder();
    gsonBuilder.setPrettyPrinting();
    Gson gson = gsonBuilder.create();
    FileContent content = noteJson.getContent();
    InputStream ins = content.getInputStream();
    String json = IOUtils.toString(ins, config.getString(NeopteryxConfig.Var.ENCODING));
    ins.close();
    return gson.fromJson(json, Note.class);
  }

  private NoteInfo getNoteInfo(FileObject noteDir) throws IOException {
    Note note = getNote(noteDir);
    return new NoteInfo(note.getId(), note.getName());
  }

  @Override
  public Note get(String noteId) throws IOException {
    FileObject rootDir = fsManager.resolveFile(getPath("/"));
    FileObject noteDir = rootDir.resolveFile(noteId, NameScope.CHILD);
    return getNote(noteDir);
  }

  private FileObject getRootDir() throws IOException {
    FileObject rootDir = fsManager.resolveFile(getPath("/"));
    if (!rootDir.exists()) {
      throw new IOException("Root path does not exists");
    }
    if (!isDirectory(rootDir)) {
      throw new IOException("Root path is not a directory");
    }
    return rootDir;
  }

  @Override
  public synchronized void save(Note note) throws IOException {
    GsonBuilder gsonBuilder = new GsonBuilder();
    gsonBuilder.setPrettyPrinting();
    Gson gson = gsonBuilder.create();
    String json = gson.toJson(note);
    FileObject rootDir = getRootDir();
    FileObject noteDir = rootDir.resolveFile(note.getId(), NameScope.CHILD);
    if (!noteDir.exists()) {
      noteDir.createFolder();
    }
    if (!isDirectory(noteDir)) {
      throw new IOException(noteDir.getName().toString() + " is not a directory");
    }
    FileObject noteJson = noteDir.resolveFile(".note.json", NameScope.CHILD);
    // false means not appending. creates file if not exists
    OutputStream out = noteJson.getContent().getOutputStream(false);
    out.write(json.getBytes(config.getString(NeopteryxConfig.Var.ENCODING)));
    out.close();
    noteJson.moveTo(noteDir.resolveFile("note.json", NameScope.CHILD));
  }
}
