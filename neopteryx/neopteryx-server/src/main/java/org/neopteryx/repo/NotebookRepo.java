package org.neopteryx.repo;

import java.io.IOException;
import java.util.List;

public interface NotebookRepo {

  List<NoteInfo> list() throws IOException;

  Note get(String id) throws IOException;

  void save(Note note) throws IOException;
}
