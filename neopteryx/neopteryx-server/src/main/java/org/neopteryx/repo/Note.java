package org.neopteryx.repo;

import java.util.List;

public class Note {

  private String id;

  private String name;

  private List<Paragraph> paragraphs;

  // private ZonedDateTime dateUpdated;

  public Note() {}

  public Note(String id, String name, List<Paragraph> paragraphs) {
    this.id = id;
    this.name = name;
    this.paragraphs = paragraphs;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public List<Paragraph> getParagraphs() {
    return paragraphs;
  }

  public void setParagraphs(List<Paragraph> paragraphs) {
    this.paragraphs = paragraphs;
  }
}
