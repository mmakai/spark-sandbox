package client.livy;

import java.util.Arrays;

public class Session {

	public Integer id;

	public SessionKind kind;

	public String[] log;

	public SessionState state;

	@Override
	public String toString() {
		return "Session [id=" + id + ", kind=" + kind + ", log=" + Arrays.toString(log) + ", state=" + state + "]";
	}
}
