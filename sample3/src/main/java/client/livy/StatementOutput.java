package client.livy;

import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

public class StatementOutput {

	public ExecutionStatus status;

	@SerializedName("execution_count")
	public int executionCount;

	public JsonObject data;

	@Override
	public String toString() {
		return "StatementOutput [status=" + status + ", executionCount=" + executionCount + ", data=" + data + "]";
	}
}
