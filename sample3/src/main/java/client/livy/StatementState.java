package client.livy;

import java.lang.reflect.Type;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public enum StatementState {

	RUNNING("running"), AVAILABLE("available"), ERROR("error");

	public final String value;

	private StatementState(String value) {
		this.value = value;
	}

	public static StatementState get(String value) {
		// TODO Do this with a nice map.
		for (StatementState s : values()) {
			if (s.value.equals(value)) {
				return s;
			}
		}
		throw new UnsupportedOperationException(value);
	}

	public static class Deserializer implements JsonDeserializer<StatementState> {

		@Override
		public StatementState deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
				throws JsonParseException {
			return StatementState.get(json.getAsString());
		}
	}

	public static class Serializer implements JsonSerializer<StatementState> {

		@Override
		public JsonElement serialize(StatementState src, Type typeOfSrc, JsonSerializationContext context) {
			return new JsonPrimitive(src.value);
		}
	}
}
