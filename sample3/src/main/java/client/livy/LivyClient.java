package client.livy;

import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.fluent.Response;
import org.apache.http.entity.ContentType;

import com.google.common.io.CharStreams;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class LivyClient {

	protected String host;

	protected GsonBuilder gsonBuilder;

	public LivyClient(String host) {
		this.host = host;
		gsonBuilder = new GsonBuilder();
		gsonBuilder.registerTypeAdapter(ExecutionStatus.class, new ExecutionStatus.Deserializer());
		gsonBuilder.registerTypeAdapter(ExecutionStatus.class, new ExecutionStatus.Serializer());
		gsonBuilder.registerTypeAdapter(SessionKind.class, new SessionKind.Deserializer());
		gsonBuilder.registerTypeAdapter(SessionKind.class, new SessionKind.Serializer());
		gsonBuilder.registerTypeAdapter(SessionState.class, new SessionState.Deserializer());
		gsonBuilder.registerTypeAdapter(SessionState.class, new SessionState.Serializer());
		gsonBuilder.registerTypeAdapter(StatementState.class, new StatementState.Deserializer());
		gsonBuilder.registerTypeAdapter(StatementState.class, new StatementState.Serializer());
	}

	private static JsonObject getJSONContent(HttpResponse response) throws IOException {
		StatusLine statusLine = response.getStatusLine();
		HttpEntity entity = response.getEntity();
		if (statusLine.getStatusCode() >= 300) {
			throw new HttpResponseException(
					statusLine.getStatusCode(),
					statusLine.getReasonPhrase());
		}
		if (entity == null) {
			throw new ClientProtocolException("Response contains no content");
		}
		ContentType contentType = ContentType.getOrDefault(entity);
		// ContentType 4.5.2 does not implement equals properly.
		if (!ContentType.APPLICATION_JSON.getCharset().equals(contentType.getCharset()) ||
				!ContentType.APPLICATION_JSON.getMimeType().equals(contentType.getMimeType())) {
			throw new ClientProtocolException("Unexpected content type:" +
					contentType);
		}
		Charset charset = contentType.getCharset();
		if (charset == null) {
			charset = Charset.forName("UTF-8");
		}
		String content = CharStreams.toString(
				new InputStreamReader(entity.getContent(), charset));
		JsonParser parser = new JsonParser();
		return parser.parse(content).getAsJsonObject();
	}

	// GET /sessions
	public List<Session> getSessions() throws IOException {
		Response response = Request.Get(host + "/sessions")
				.addHeader("Content-Type", "application/json")
				.execute();
		List<Session> sessions = response.handleResponse(new ResponseHandler<List<Session>>() {

			@Override
			public List<Session> handleResponse(HttpResponse response) throws IOException {
				List<Session> sessions = new ArrayList<>();
				JsonObject json = getJSONContent(response);
				// TODO LOG.
				System.out.println(json);
				// NOTE provisorically ignore from and total fields.
				JsonArray array = json.getAsJsonArray("sessions");
				array.forEach(new Consumer<JsonElement>() {

					@Override
					public void accept(JsonElement t) {
						JsonObject json = t.getAsJsonObject();
						Gson gson = gsonBuilder.create();
						Session session = gson.fromJson(json, Session.class);
						sessions.add(session);
					}
				});
				return sessions;
			}
		});
		System.out.println(sessions);
		return sessions;
	}

	// POST /sessions
	public Session createSession(Session session) throws IOException {
		Gson gson = gsonBuilder.create();
		String json = gson.toJson(session);
		System.out.println(json);
		Response response = Request.Post(host + "/sessions")
				.addHeader("Content-Type", "application/json")
				.bodyString(json, ContentType.APPLICATION_JSON)
				.execute();
		Session tmp = response.handleResponse(new ResponseHandler<Session>() {

			@Override
			public Session handleResponse(HttpResponse response) throws IOException {
				JsonObject json = getJSONContent(response);
				// TODO LOG.
				System.out.println(json);
				Gson gson = gsonBuilder.create();
				return gson.fromJson(json, Session.class);
			}
		});
		System.out.println(tmp);
		return tmp;
	}

	// TODO
	//GET /sessions/{sessionId}

	// DELETE /sessions/{sessionId}
	public void deleteSession(int id) throws IOException {
		Response response = Request.Delete(host + "/sessions/" + id)
				.addHeader("Content-Type", "application/json")
				.execute();
		response.handleResponse(new ResponseHandler<Void>() {

			@Override
			public Void handleResponse(HttpResponse response) throws IOException {
				JsonObject json = getJSONContent(response);
				// TODO LOG.
				System.out.println(json);
				return null;
			}
		});
	}

	// GET /sessions/{sessionId}/logs
	public Session getSessionLogs(int id) throws IOException {
		Response response = Request.Get(host + "/sessions/" + id + "/logs")
				.addHeader("Content-Type", "application/json")
				.execute();
		Session tmp = response.handleResponse(new ResponseHandler<Session>() {

			@Override
			public Session handleResponse(HttpResponse response) throws IOException {
				JsonObject json = getJSONContent(response);
				// TODO LOG.
				System.out.println(json);
				return null;
			}
		});
		System.out.println(tmp);
		return tmp;
	}

//	//GET /sessions/{sessionId}/statements
//	public List<Statement> getStatements(int sessionId) throws IOException {
//		Response response = Request.Get(host + "/sessions/" + sessionId + "/statements")
//				.addHeader("Content-Type", "application/json")
//				.execute();
//		List<Statement> statements = response.handleResponse(new ResponseHandler<List<Statement>>() {
//
//			@Override
//			public List<Statement> handleResponse(HttpResponse response) throws IOException {
//				List<Statement> statements = new ArrayList<>();
//				JsonObject json = getJSONContent(response);
//				// TODO LOG.
//				System.out.println(json);
//				JsonArray array = json.getAsJsonArray("sessions");
//				array.forEach(new Consumer<JsonElement>() {
//
//					@Override
//					public void accept(JsonElement t) {
//						JsonObject json = t.getAsJsonObject();
//						Statement statement = new Statement();
//						statement.id = json.get("id").getAsInt();
//						statement.state = StatementState.get(json.get("state").getAsString());
//						if (json.get("output").isJsonObject()) {
//							JsonObject output = json.getAsJsonObject("output");
//							statement.output = new StatementOutput();
//							if (json.get("data").isJsonObject()) {
//								statement.output.data = output.getAsJsonObject("data");
//							}
//							statement.output.execution_count = output.get("execution_count").getAsInt();
//							statement.output.status = ExecutionStatus.get(output.get("status").getAsString());
//						}
//						statements.add(statement);
//					}
//				});
//				return statements;
//			}
//		});
//		return statements;
//	}

	//GET /sessions/{sessionId}/statements/{statementId}
	public Statement getStatement(int sessionId, int statementId) throws IOException {
		Response response = Request.Get(host + "/sessions/" + sessionId + "/statements/" + statementId)
				.addHeader("Content-Type", "application/json")
				.execute();
		Statement tmp = response.handleResponse(new ResponseHandler<Statement>() {

			@Override
			public Statement handleResponse(HttpResponse response) throws IOException {
				JsonObject json = getJSONContent(response);
				// TODO LOG.
				System.out.println(json);
				Gson gson = gsonBuilder.create();
				Statement statement = gson.fromJson(json, Statement.class);
				return statement;
			}
		});
		System.out.println(tmp);
		return tmp;
	}

	// POST /sessions/{sessionId}/statements
	public Statement createStatement(int sessionId, String code) throws IOException {
		JsonObject json = new JsonObject();
		json.addProperty("code", code);
		Response response = Request.Post(host + "/sessions/" + sessionId + "/statements")
				.addHeader("Content-Type", "application/json")
				.bodyString(json.toString(), ContentType.APPLICATION_JSON)
				.execute();
		Statement tmp = response.handleResponse(new ResponseHandler<Statement>() {

			@Override
			public Statement handleResponse(HttpResponse response) throws IOException {
				JsonObject json = getJSONContent(response);
				// TODO LOG.
				System.out.println(json);
				Gson gson = gsonBuilder.create();
				Statement statement = gson.fromJson(json, Statement.class);
				return statement;
			}
		});
		System.out.println(tmp);
		return tmp;
	}
}
