package client.livy;

import java.lang.reflect.Type;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public enum SessionState {

	NOT_STARTED("not_started"), STARTING("starting"), IDLE("idle"),
	BUSY("busy"), ERROR("error"), DEAD("dead");

	public final String value;

	private SessionState(String value) {
		this.value = value;
	}

	public static SessionState get(String value) {
		// TODO Do this with a nice map.
		for (SessionState s : values()) {
			if (s.value.equals(value)) {
				return s;
			}
		}
		throw new UnsupportedOperationException(value);
	}

	public static class Deserializer implements JsonDeserializer<SessionState> {

		@Override
		public SessionState deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
				throws JsonParseException {
			return SessionState.get(json.getAsString());
		}
	}

	public static class Serializer implements JsonSerializer<SessionState> {

		@Override
		public JsonElement serialize(SessionState src, Type typeOfSrc, JsonSerializationContext context) {
			return new JsonPrimitive(src.value);
		}
	}
}
