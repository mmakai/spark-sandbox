package client.livy;

import java.lang.reflect.Type;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

// TODO Should we name this as StatementExecutionStatus for convention?
public enum ExecutionStatus {

	OK("ok"), ERROR("error");

	public final String value;

	private ExecutionStatus(String value) {
		this.value = value;
	}

	public static ExecutionStatus get(String value) {
		// TODO Do this with a nice map.
		for (ExecutionStatus s : values()) {
			if (s.value.equals(value)) {
				return s;
			}
		}
		throw new UnsupportedOperationException(value);
	}

	public static class Deserializer implements JsonDeserializer<ExecutionStatus> {

		@Override
		public ExecutionStatus deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
				throws JsonParseException {
			return ExecutionStatus.get(json.getAsString());
		}
	}

	public static class Serializer implements JsonSerializer<ExecutionStatus> {

		@Override
		public JsonElement serialize(ExecutionStatus src, Type typeOfSrc, JsonSerializationContext context) {
			return new JsonPrimitive(src.value);
		}
	}
}
