package client.livy;

import java.lang.reflect.Type;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public enum SessionKind {

	SPARK("spark"), PY_SPARK("pyspark"), SPARK_R("sparkr");

	public final String value;

	private SessionKind(String value) {
		this.value = value;
	}

	public static SessionKind get(String value) {
		// TODO Do this with a nice map.
		for (SessionKind k : values()) {
			if (k.value.equals(value)) {
				return k;
			}
		}
		throw new UnsupportedOperationException(value);
	}

	public static class Deserializer implements JsonDeserializer<SessionKind> {

		@Override
		public SessionKind deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
				throws JsonParseException {
			return SessionKind.get(json.getAsString());
		}
	}

	public static class Serializer implements JsonSerializer<SessionKind> {

		@Override
		public JsonElement serialize(SessionKind src, Type typeOfSrc, JsonSerializationContext context) {
			return new JsonPrimitive(src.value);
		}
	}
}
