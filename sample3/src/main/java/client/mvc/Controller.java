package client.mvc;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;

public abstract class Controller implements Initializable {

	//protected String fxml;

	@FXML
	protected Node root;

	// TODO Non-FXML constructor.

	//public Controller(String fxml) {
	//	this.fxml = fxml;
	//}

	/* (non-Javadoc)
	 * @see javafx.fxml.Initializable#initialize(java.net.URL, java.util.ResourceBundle)
	 */
	public abstract void initialize(URL location, ResourceBundle resources);

	/**
	 * An implementation of this method should perform the following clean up
	 * tasks. Clean up the child views of the view corresponding to this
	 * controller by recursively calling {@link #unInitialize()} of their
	 * controllers. Bindings of all models external to the controller should be
	 * removed in.
	 */
	public abstract void unInitialize();
}
