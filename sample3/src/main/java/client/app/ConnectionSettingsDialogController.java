package client.app;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import client.app.ConnectionSettingsDialogModel.UISessionState;
import client.livy.LivyClient;
import client.livy.Session;
import client.livy.SessionKind;
import client.mvc.Controller;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.beans.value.WeakChangeListener;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.util.Duration;

public class ConnectionSettingsDialogController extends Controller {

	// TODO inject
	protected ConnectionSettingsDialogModel connectionSettingsDialogModel =
			new ConnectionSettingsDialogModel();

	protected Timeline timeline;

	@FXML
	private TextField serverAddressTextField;

	@FXML
	private Button connectButton;

	@FXML
	private TextArea messageTextArea;

	@FXML
	private Label sessionsLabel;

	@FXML
	private ComboBox<UISessionState> sessionsComboBox;

	@FXML
	private Button createSessionButton;

	@FXML
	private void connectAction(ActionEvent event) {
		sessionsLabel.setDisable(true);
		sessionsComboBox.setDisable(true);
		createSessionButton.setDisable(true);
		timeline.play();
	}

	@FXML
	private void createSessionAction(ActionEvent event) {
		LivyClient client = new LivyClient(connectionSettingsDialogModel.serverAddress.get());
		Session session = new Session();
		session.kind = SessionKind.SPARK;
		try {
			Session result = client.createSession(session);
			session.id = result.id;
			session.state = result.state;
			sessionsComboBox.getItems().removeIf(s -> s.id == session.id);
			UISessionState s = new UISessionState();
			s.id = session.id;
			s.state = session.state;
			sessionsComboBox.getItems().add(s);
			sessionsComboBox.getItems().sort((s1, s2) -> Integer.compare(s1.id, s2.id));
			// If the selection is empty, then one element is selected.
			if (sessionsComboBox.getSelectionModel().getSelectedIndex() < 0 &&
					!sessionsComboBox.getItems().isEmpty()) {
				sessionsComboBox.getSelectionModel().select(0);
			}
		} catch (IOException e) {
			StringWriter stringWriter = new StringWriter();
			PrintWriter printWriter = new PrintWriter(stringWriter);
			e.printStackTrace(printWriter);
			messageTextArea.setText(stringWriter.toString());
		}
	}

	private ChangeListener<UISessionState> sessionListener =
			(ObservableValue<? extends UISessionState> observable, UISessionState oldValue,
					UISessionState newValue) -> {
						System.out.println("B " + oldValue + " " + newValue);
					};

	private ChangeListener<UISessionState> comboBoxListener =
			(ObservableValue<? extends UISessionState> observable, UISessionState oldValue,
					UISessionState newValue) -> {
						connectionSettingsDialogModel.session.set(newValue);
						System.out.println("A " + newValue);
					};

	public ConnectionSettingsDialogController() {
		timeline = new Timeline(new KeyFrame(
				Duration.millis(1000),
				event -> {
					sessionsLabel.setDisable(false);
					sessionsComboBox.setDisable(false);
					createSessionButton.setDisable(false);
					updateSessions();
					timeline.playFromStart();
				}));
	}

	private void updateSessions() {
		LivyClient client = new LivyClient(connectionSettingsDialogModel.serverAddress.get());
		try {
			List<UISessionState> ids = new ArrayList<>();
			client.getSessions().forEach(session -> {
				UISessionState s = new UISessionState();
				s.id = session.id;
				s.state = session.state;
				ids.add(s);
			});
			ids.sort((s1, s2) -> Integer.compare(s1.id, s2.id));
			UISessionState selected = sessionsComboBox.getSelectionModel().getSelectedItem();
			sessionsComboBox.getItems().clear();
			sessionsComboBox.getItems().addAll(ids);
			// If there is a selected element, and a new one with matching id,
			// then the new one is selected.
			if (selected != null) {
				ids.forEach(s -> {
					if (s.id == selected.id) {
						sessionsComboBox.getSelectionModel().select(s);
					}
				});
			};
			// If the selection is empty, then one element is selected.
			if (sessionsComboBox.getSelectionModel().getSelectedIndex() < 0 &&
					!sessionsComboBox.getItems().isEmpty()) {
				sessionsComboBox.getSelectionModel().select(0);
			}
			messageTextArea.clear();
		} catch (IOException e) {
			StringWriter stringWriter = new StringWriter();
			PrintWriter printWriter = new PrintWriter(stringWriter);
			e.printStackTrace(printWriter);
			messageTextArea.setText(stringWriter.toString());
		}
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		serverAddressTextField.textProperty().bindBidirectional(connectionSettingsDialogModel.serverAddress);
		connectionSettingsDialogModel.serverAddress.set("http://localhost:8998");
		connectionSettingsDialogModel.session.addListener(sessionListener);
		sessionsComboBox.getSelectionModel().selectedItemProperty().addListener(new WeakChangeListener<>(
				comboBoxListener));
		serverAddressTextField.widthProperty().addListener(new WeakChangeListener<>(
				(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
					sessionsComboBox.setPrefWidth(newValue.doubleValue());
				}));
		connectButton.widthProperty().addListener(new WeakChangeListener<>(
				(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
					createSessionButton.setPrefWidth(newValue.doubleValue());
				}));
	}

	@Override
	public void unInitialize() {
		timeline.stop();
		serverAddressTextField.textProperty().unbindBidirectional(connectionSettingsDialogModel.serverAddress);
		connectionSettingsDialogModel.session.removeListener(sessionListener);
		System.out.println("uninit " + this);
	}
}
