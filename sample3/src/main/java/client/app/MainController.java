package client.app;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import client.mvc.Controller;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;

public class MainController extends Controller {

	// TODO inject
	protected MainModel mainModel = new MainModel();

	// TODO inject
	protected ConnectionModel connectionModel;

	@FXML
	private VBox statusBar;

	@FXML
	private Label resultLabel;

	@FXML
	private Button actionButton;

	@FXML
	private void buttonAction(ActionEvent event) {
		mainModel.clickCount.set(mainModel.clickCount.get() + 1);
	}

	private FXMLLoader statusLoader;

	@FXML
	private VBox processorsVBox;

	private List<FXMLLoader> processorLoaders = new ArrayList<>();

	private ChangeListener<Number> clickCountListener =
			(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
				resultLabel.setText("Button was clicked " + mainModel.clickCount.get() + " times");
			};

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		mainModel.clickCount.addListener(clickCountListener);
		resultLabel.setText("Button was clicked " + mainModel.clickCount.get() + " times");
		try {
			statusLoader = new FXMLLoader(getClass().getResource("/ConnectionStatusView.fxml"));
			ConnectionStatusController statusController = new ConnectionStatusController();
			statusController.connectionModel = connectionModel;
			statusLoader.setController(statusController);
			Parent statusView = statusLoader.load();
			statusBar.getChildren().add(statusView);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for (int i = 0; i < 3; ++i) {
			try {
				FXMLLoader processorLoader = new FXMLLoader(getClass().getResource("/ProcessorView.fxml"));
				ProcessorController processorController = new ProcessorController();
				processorController.connectionModel = connectionModel;
				processorLoader.setController(processorController);
				Parent processorView = processorLoader.load();
				processorLoaders.add(processorLoader);
				processorsVBox.getChildren().add(processorView);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	@Override
	public void unInitialize() {
		mainModel.clickCount.removeListener(clickCountListener);
		statusLoader.<Controller>getController().unInitialize();
		processorLoaders.forEach(loader -> loader.<Controller>getController().unInitialize());
		System.out.println("uninit " + this);
	}

	//  TODO Close listener
}
