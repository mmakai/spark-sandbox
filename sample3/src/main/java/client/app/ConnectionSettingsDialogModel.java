package client.app;

import client.livy.SessionState;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class ConnectionSettingsDialogModel {

	public static class UISessionState {

		public int id;

		public SessionState state;

		@Override
		public String toString() {
			return id + " (" + state.value + ")";
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + id;
			result = prime * result + ((state == null) ? 0 : state.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			UISessionState other = (UISessionState) obj;
			if (id != other.id)
				return false;
			if (state != other.state)
				return false;
			return true;
		}
	}

	public StringProperty serverAddress = new SimpleStringProperty();

	//public ObservableList<UISessionState> sessions = new SimpleListProperty<>();

	public ObjectProperty<UISessionState> session =
			new SimpleObjectProperty<UISessionState>();
}
