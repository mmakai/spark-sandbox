package client.app;

import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

import client.mvc.Controller;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.TextField;
import javafx.util.Callback;

public class ConnectionStatusController extends Controller {

	// TODO inject
	protected ConnectionModel connectionModel;

	@FXML
	private TextField serverAddressTextField;

	@FXML
	private TextField sessionTextField;

	@FXML
	private Button connectionSettingsButton;

	@FXML
	private void connectionSettingsButtonAction(ActionEvent event) {
		Dialog<ConnectionSettingsDialogModel> dialog = new Dialog<>();
		try {
			FXMLLoader dialogLoader = new FXMLLoader(getClass().getResource("/ConnectionSettingsDialogView.fxml"));
			ConnectionSettingsDialogController dialogController =
					new ConnectionSettingsDialogController();
			dialogLoader.setController(dialogController);
			Parent dialogView = dialogLoader.load();
			dialog.getDialogPane().setContent(dialogView);
			dialog.setTitle("Connection Settings");
			dialog.setResizable(true);
			ButtonType buttonTypeOk = new ButtonType("OK", ButtonData.OK_DONE);
			ButtonType buttonTypeCancel = new ButtonType("Cancel", ButtonData.CANCEL_CLOSE);
			dialog.getDialogPane().getButtonTypes().add(buttonTypeOk);
			dialog.getDialogPane().getButtonTypes().add(buttonTypeCancel);
			dialog.setOnHiding(event1 -> {
				dialogLoader.<Controller>getController().unInitialize();
			});
			dialog.setResultConverter(new Callback<ButtonType, ConnectionSettingsDialogModel>() {

				@Override
				public ConnectionSettingsDialogModel call(ButtonType param) {
					switch (param.getButtonData()) {
					case OK_DONE:
						return dialogLoader.<ConnectionSettingsDialogController>getController().connectionSettingsDialogModel;
					case CANCEL_CLOSE:
						return null;
					default:
						throw new UnsupportedOperationException();
					}
				}
			});
			Optional<ConnectionSettingsDialogModel> result = dialog.showAndWait();
			if (result.isPresent()) {
				connectionModel.serverAddress.set(result.get().serverAddress.get());
				serverAddressTextField.setText(result.get().serverAddress.get());
				connectionModel.session.set(result.get().session.get());
				System.out.println(result.get().session.get());
				sessionTextField.setText("" + result.get().session.get().id);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("click");
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		sessionTextField.prefColumnCountProperty().set(TextField.DEFAULT_PREF_COLUMN_COUNT / 2);
	}

	@Override
	public void unInitialize() {
		System.out.println("uninit " + this);
	}

	// TODO Close listener
}
