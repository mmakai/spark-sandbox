package client.app;

import java.io.IOException;

import client.mvc.Controller;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

	public static void main(String[] args) throws Exception {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws IOException {
		FXMLLoader mainLoader = new FXMLLoader(getClass().getResource("/MainView.fxml"));
		MainController mainController = new MainController();
		mainController.connectionModel = new ConnectionModel();
		mainLoader.setController(mainController);
		Parent mainView = mainLoader.load();
		primaryStage.setOnHiding(event -> {
			System.out.println("hiding");
			mainLoader.<Controller>getController().unInitialize();
		});
		Scene scene = new Scene(mainView, 480, 360);
		primaryStage.setTitle("Hello World!");
		primaryStage.setScene(scene);
		primaryStage.show();
	}
}
