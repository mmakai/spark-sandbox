package client.app;

import client.app.ConnectionSettingsDialogModel.UISessionState;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class ConnectionModel {

	public StringProperty serverAddress = new SimpleStringProperty();

	public ObjectProperty<UISessionState> session =
			new SimpleObjectProperty<UISessionState>();
}
