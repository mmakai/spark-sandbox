package client.app;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import client.livy.LivyClient;
import client.livy.Statement;
import client.mvc.Controller;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.util.Duration;

public class ProcessorController extends Controller {

	protected ConnectionModel connectionModel;

	protected Statement statement = null;

	protected Timeline timeline;

	@FXML
	private TextArea codeTextArea;

	@FXML
	private Button runButton;

	@FXML
	private Label stateLabel;

	@FXML
	private void runButtonAction(ActionEvent event) {
		LivyClient client = new LivyClient(connectionModel.serverAddress.get());
		try {
			statement = client.createStatement(connectionModel.session.get().id, codeTextArea.getText());
			stateLabel.setText(statement.state.value);
			runButton.setDisable(true);
			codeTextArea.setEditable(false);
			switch (statement.state) {
			case AVAILABLE:
				switch (statement.output.status) {
				case ERROR:
					// TODO
					resultTextArea.setText("error");
					break;
				case OK:
					resultTextArea.setText(statement.output.data.toString());
					break;
				default:
					throw new UnsupportedOperationException();
				}
				break;
			case ERROR:
				switch (statement.output.status) {
				case ERROR:
					// TODO
					resultTextArea.setText("error");
					break;
				case OK:
					resultTextArea.setText(statement.output.data.toString());
					break;
				default:
					throw new UnsupportedOperationException();
				}
				break;
			case RUNNING:
				timeline.play();
				break;
			default:
				throw new UnsupportedOperationException();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@FXML
	private TextArea resultTextArea;

	public ProcessorController() {
		timeline = new Timeline(new KeyFrame(
				Duration.millis(1000),
				event -> updateResult()
				));
	}

	private void updateResult() {
		LivyClient client = new LivyClient(connectionModel.serverAddress.get());
		try {
			Statement result = client.getStatement(connectionModel.session.get().id, statement.id);
			statement.state = result.state;
			statement.output = result.output;
			stateLabel.setText(statement.state.value);
			switch (statement.state) {
			case AVAILABLE:
				switch (statement.output.status) {
				case ERROR:
					// TODO
					resultTextArea.setText("error");
					break;
				case OK:
					resultTextArea.setText(statement.output.data.toString());
					break;
				default:
					throw new UnsupportedOperationException();
				}
				break;
			case ERROR:
				switch (statement.output.status) {
				case ERROR:
					// TODO
					resultTextArea.setText("error");
					break;
				case OK:
					resultTextArea.setText(statement.output.data.toString());
					break;
				default:
					throw new UnsupportedOperationException();
				}
				break;
			case RUNNING:
				timeline.playFromStart();
				break;
			default:
				throw new UnsupportedOperationException();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
	}

	@Override
	public void unInitialize() {
		System.out.println("uninit " + this);
	}
}
