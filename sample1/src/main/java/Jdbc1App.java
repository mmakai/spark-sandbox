import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.rdd.JdbcRDD;

import scala.reflect.ClassManifestFactory$;
import scala.runtime.AbstractFunction0;
import scala.runtime.AbstractFunction1;

public class Jdbc1App {

	private static final String MYSQL_DRIVER = "com.mysql.jdbc.Driver";
	private static final String MYSQL_CONNECTION_URL = "jdbc:mysql://localhost:3306/enron_v5";
	private static final String MYSQL_USERNAME = "root";
	private static final String MYSQL_PWD = "padmin";

	@SuppressWarnings("serial")
	public static class DbConnection extends AbstractFunction0<Connection> implements Serializable {

		private String driverClassName;
		private String connectionUrl;
		private String userName;
		private String password;

		public DbConnection(String driverClassName, String connectionUrl, String userName, String password) {
			this.driverClassName = driverClassName;
			this.connectionUrl = connectionUrl;
			this.userName = userName;
			this.password = password;
		}

		@Override
		public Connection apply() {
			try {
				Class.forName(driverClassName);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
				//LOGGER.error("Failed to load driver class", e);
			}
			Properties properties = new Properties();
			properties.setProperty("user", userName);
			properties.setProperty("password", password);
			Connection connection = null;
			try {
				connection = DriverManager.getConnection(connectionUrl, properties);
			} catch (SQLException e) {
				e.printStackTrace();
				//LOGGER.error("Connection failed", e);
			}
			return connection;
		}
	}

	@SuppressWarnings("serial")
	static class MapResult extends AbstractFunction1<ResultSet, Object[]> implements Serializable {

		@Override
		public Object[] apply(ResultSet row) {
			return JdbcRDD.resultSetToObjectArray(row);
		}
	}

	public static void main(String[] args) {
		SparkConf conf = new SparkConf().setAppName("Jdbc1 Application");
		JavaSparkContext sc = new JavaSparkContext(conf);
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		DbConnection dbConnection = new DbConnection(MYSQL_DRIVER, MYSQL_CONNECTION_URL, MYSQL_USERNAME, MYSQL_PWD);
		JdbcRDD<Object[]> jdbcRDD = new JdbcRDD<>(sc.sc(), dbConnection, "SELECT * FROM employeelist WHERE ? = ?", 0, 0, 1, new MapResult(), ClassManifestFactory$.MODULE$.fromClass(Object[].class));
		JavaRDD<Object[]> javaRDD1 = JavaRDD.fromRDD(jdbcRDD, ClassManifestFactory$.MODULE$.fromClass(Object[].class));
		JavaRDD<Object[]> javaRDD2 = javaRDD1.coalesce(8);
		long numAs = javaRDD2.count();

		System.out.println("Employees: " + numAs);
	}
}
