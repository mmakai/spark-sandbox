import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Properties;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.rdd.JdbcRDD;

import com.clearspring.analytics.util.Lists;
import com.google.common.base.Joiner;

import scala.Tuple2;
import scala.reflect.ClassManifestFactory$;
import scala.runtime.AbstractFunction0;
import scala.runtime.AbstractFunction1;

public class Jdbc2App {

	private static final String MYSQL_DRIVER = "com.mysql.jdbc.Driver";
	private static final String MYSQL_CONNECTION_URL = "jdbc:mysql://localhost:3306/enron_v5";
	private static final String MYSQL_USERNAME = "root";
	private static final String MYSQL_PWD = "padmin";

	@SuppressWarnings("serial")
	public static class DbConnection extends AbstractFunction0<Connection> implements Serializable {

		private String driverClassName;
		private String connectionUrl;
		private String userName;
		private String password;

		public DbConnection(String driverClassName, String connectionUrl, String userName, String password) {
			this.driverClassName = driverClassName;
			this.connectionUrl = connectionUrl;
			this.userName = userName;
			this.password = password;
		}

		@Override
		public Connection apply() {
			try {
				Class.forName(driverClassName);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
				//LOGGER.error("Failed to load driver class", e);
			}
			Properties properties = new Properties();
			properties.setProperty("user", userName);
			properties.setProperty("password", password);
			Connection connection = null;
			try {
				connection = DriverManager.getConnection(connectionUrl, properties);
			} catch (SQLException e) {
				e.printStackTrace();
				//LOGGER.error("Connection failed", e);
			}
			return connection;
		}
	}

	@SuppressWarnings("serial")
	static class MapResult extends AbstractFunction1<ResultSet, Object[]> implements Serializable {

		@Override
		public Object[] apply(ResultSet row) {
			return JdbcRDD.resultSetToObjectArray(row);
		}
	}

	public static void main(String[] args) {
		SparkConf conf = new SparkConf().setAppName("Jdbc2 Application");
		JavaSparkContext sc = new JavaSparkContext(conf);
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		DbConnection dbConnection = new DbConnection(MYSQL_DRIVER, MYSQL_CONNECTION_URL, MYSQL_USERNAME, MYSQL_PWD);
		JdbcRDD<Object[]> jdbcRDD = new JdbcRDD<>(sc.sc(), dbConnection, "SELECT sender FROM edges3 WHERE (mid % 8) >= ? AND (mid % 8) <= ?", 0, 8, 8, new MapResult(), ClassManifestFactory$.MODULE$.fromClass(Object[].class));
		JavaRDD<Object[]> javaRDD = JavaRDD.fromRDD(jdbcRDD, ClassManifestFactory$.MODULE$.fromClass(Object[].class));
		@SuppressWarnings("serial")
		JavaPairRDD<String, Integer> senderRDD = javaRDD.mapToPair(new PairFunction<Object[], String, Integer>() {

			@Override
			public Tuple2<String, Integer> call(Object[] t) throws Exception {
				String s = (String) t[0];
				return new Tuple2<String, Integer>(s, 1);
			}
		});
		@SuppressWarnings("serial")
		JavaPairRDD<String, Integer> outDegreeRDD = senderRDD.reduceByKey(new Function2<Integer, Integer, Integer>() {

			@Override
			public Integer call(Integer v1, Integer v2) throws Exception {
				return v1 + v2;
			}
		});
		// The sort does not work with the output of the collect.
		List<Tuple2<String, Integer>> outDegrees = Lists.newArrayList(outDegreeRDD.collect());
		// TODO Is not there a simple function to create this comparator?
		Collections.sort(outDegrees, new Comparator<Tuple2<String, Integer>>() {

			@Override
			public int compare(Tuple2<String, Integer> o1, Tuple2<String, Integer> o2) {
				return o1._1.compareTo(o2._1);
			}
		});

		System.out.println("Outdegrees: " + Joiner.on('\n').join(outDegrees));
	}
}
