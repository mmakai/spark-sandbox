package client.livy;

import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.fluent.Response;
import org.apache.http.entity.ContentType;
import org.json.JSONArray;
import org.json.JSONObject;

import com.google.common.io.CharStreams;

public class LivyClient {

	protected String host;

	public LivyClient(String host) {
		this.host = host;
	}

	private static JSONObject getJSONContent(HttpResponse response) throws IOException {
		StatusLine statusLine = response.getStatusLine();
		HttpEntity entity = response.getEntity();
		if (statusLine.getStatusCode() >= 300) {
			throw new HttpResponseException(
					statusLine.getStatusCode(),
					statusLine.getReasonPhrase());
		}
		if (entity == null) {
			throw new ClientProtocolException("Response contains no content");
		}
		ContentType contentType = ContentType.getOrDefault(entity);
		// ContentType 4.5.2 does not implement equals properly.
		if (!ContentType.APPLICATION_JSON.getCharset().equals(contentType.getCharset()) ||
				!ContentType.APPLICATION_JSON.getMimeType().equals(contentType.getMimeType())) {							
			throw new ClientProtocolException("Unexpected content type:" +
					contentType);
		}
		Charset charset = contentType.getCharset();
		if (charset == null) {
			charset = Charset.forName("UTF-8");
		}
		String content = CharStreams.toString(
				new InputStreamReader(entity.getContent(), charset));
		return new JSONObject(content);
	}

	// GET /sessions
	public List<Session> getSessions() throws IOException {
		Response response = Request.Get(host + "/sessions")
				.addHeader("Content-Type", "application/json")
				.execute();
		List<Session> sessions = response.handleResponse(new ResponseHandler<List<Session>>() {

			@Override
			public List<Session> handleResponse(HttpResponse response) throws IOException {
				List<Session> sessions = new ArrayList<>();
				JSONObject json = getJSONContent(response);
				// TODO LOG.
				System.out.println(json);
				JSONArray array = json.getJSONArray("sessions");
				array.forEach(new Consumer<Object>() {

					@Override
					public void accept(Object t) {
						JSONObject json = (JSONObject) t;
						Session session = new Session();
						session.id = json.getInt("id");
						session.kind = SessionKind.get(json.getString("kind"));
						session.state = SessionState.get(json.getString("state"));
						sessions.add(session);
					}
				});
				return sessions;
			}
		});
		return sessions;
	}

	// POST /sessions
	public void createSession(Session session) throws IOException {
		JSONObject json = new JSONObject();
		json.put("kind", session.kind.kind);
		Response response = Request.Post(host + "/sessions")
				.addHeader("Content-Type", "application/json")
				.bodyString(json.toString(), ContentType.APPLICATION_JSON)
				.execute();
		response.handleResponse(new ResponseHandler<Void>() {

			@Override
			public Void handleResponse(HttpResponse response) throws IOException {
				JSONObject json = getJSONContent(response);
				// TODO LOG.
				System.out.println(json);
				session.id = json.getInt("id");
				session.kind = SessionKind.get(json.getString("kind"));
				session.state = SessionState.get(json.getString("state"));
				return null;
			}
		});
	}

	// TODO
	//GET /sessions/{sessionId}

	// DELETE /sessions/{sessionId}
	public void deleteSession(int id) throws IOException {
		Response response = Request.Delete(host + "/sessions/" + id)
				.addHeader("Content-Type", "application/json")
				.execute();
		response.handleResponse(new ResponseHandler<Void>() {

			@Override
			public Void handleResponse(HttpResponse response) throws IOException {
				JSONObject json = getJSONContent(response);
				// TODO LOG.
				System.out.println(json);
				return null;
			}
		});
	}

	// GET /sessions/{sessionId}/logs
	public Session getSessionLogs(int id) throws IOException {
		Response response = Request.Get(host + "/sessions/" + id + "/logs")
				.addHeader("Content-Type", "application/json")
				.execute();
		Session session = response.handleResponse(new ResponseHandler<Session>() {

			@Override
			public Session handleResponse(HttpResponse response) throws IOException {
				JSONObject json = getJSONContent(response);
				// TODO LOG.
				System.out.println(json);
				return null;
//				JSONArray array = json.getJSONArray("sessions");
//				Session sessions = new Session();
//				array.forEach(new Consumer<Object>() {
//
//					@Override
//					public void accept(Object t) {
//						JSONObject json = (JSONObject) t;
//						Session session = new Session();
//						session.id = json.getInt("id");
//						session.kind = Kind.get(json.getString("kind"));
//						session.state = State.get(json.getString("state"));
//						sessions.add(session);
//					}
//				});
//				return session;
			}
		});
		return session;
	}
	
	//GET /sessions/{sessionId}/statements
	public List<Statement> getStatements(int sessionId) throws IOException {
		Response response = Request.Get(host + "/sessions/" + sessionId + "/statements")
				.addHeader("Content-Type", "application/json")
				.execute();
		List<Statement> statements = response.handleResponse(new ResponseHandler<List<Statement>>() {

			@Override
			public List<Statement> handleResponse(HttpResponse response) throws IOException {
				List<Statement> statements = new ArrayList<>();
				//System.out.println(Arrays.toString(response.getAllHeaders()));
				JSONObject json = getJSONContent(response);
				// TODO LOG.
				System.out.println(json);
				JSONArray array = json.getJSONArray("sessions");
				array.forEach(new Consumer<Object>() {

					@Override
					public void accept(Object t) {
						JSONObject json = (JSONObject) t;
						Statement statement = new Statement();
						statement.id = json.getInt("id");
						statement.state = StatementState.get(json.getString("state"));
						statements.add(statement);
					}
				});
				//statement.id = json.getInt("id");
				//statement.state = StatementState.get(json.getString("state"));
				//session.kind = Kind.get(json.getString("kind"));
				//session.state = State.get(json.getString("state"));
				return statements;
			}
		});
		return statements;
	}

	// POST /sessions/{sessionId}/statements
	public Statement createStatement(int sessionId, String code) throws IOException {
		JSONObject json = new JSONObject();
		json.put("code", code);
		Response response = Request.Post(host + "/sessions/" + sessionId + "/statements")
				.addHeader("Content-Type", "application/json")
				.bodyString(json.toString(), ContentType.APPLICATION_JSON)
				.execute();
		Statement statement = response.handleResponse(new ResponseHandler<Statement>() {

			@Override
			public Statement handleResponse(HttpResponse response) throws IOException {
				Statement statement = new Statement();
				//System.out.println(Arrays.toString(response.getAllHeaders()));
				JSONObject json = getJSONContent(response);
				// TODO LOG.
				System.out.println(json);
				statement.id = json.getInt("id");
				statement.state = StatementState.get(json.getString("state"));
				//session.kind = Kind.get(json.getString("kind"));
				//session.state = State.get(json.getString("state"));
				return statement;
			}
		});
		return statement;
	}
}
