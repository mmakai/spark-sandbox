package client.livy;

public enum StatementState {

	RUNNING("running"), AVAILABLE("available"), ERROR("error");

	public final String state;

	private StatementState(String state) {
		this.state = state;
	}

	public static StatementState get(String state) {
		// TODO Do this with a nice map.
		for (StatementState s : values()) {
			if (s.state.equals(state)) {
				return s;
			}
		}
		throw new UnsupportedOperationException();
	}
}
