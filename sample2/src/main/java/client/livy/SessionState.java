package client.livy;

public enum SessionState {

	NOT_STARTED("not_started"), STARTING("starting"), IDLE("idle"),
	BUSY("busy"), ERROR("error"), DEAD("dead");

	public final String state;

	private SessionState(String state) {
		this.state = state;
	}

	public static SessionState get(String state) {
		// TODO Do this with a nice map.
		for (SessionState s : values()) {
			if (s.state.equals(state)) {
				return s;
			}
		}
		throw new UnsupportedOperationException();
	}
}
