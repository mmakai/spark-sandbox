package client.livy;

public class Statement {

	public int id;

	public StatementState state;

	@Override
	public String toString() {
		return "Statement [id=" + id + ", state=" + state + "]";
	}
}
