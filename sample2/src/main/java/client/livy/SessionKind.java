package client.livy;

public enum SessionKind {

	SPARK("spark"), PY_SPARK("pyspark"), SPARK_R("sparkr");

	public final String kind;

	private SessionKind(String kind) {
		this.kind = kind;
	}

	public static SessionKind get(String kind) {
		// TODO Do this with a nice map.
		for (SessionKind k : values()) {
			if (k.kind.equals(kind)) {
				return k;
			}
		}
		throw new UnsupportedOperationException();
	}
}
