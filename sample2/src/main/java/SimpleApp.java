import java.io.IOException;
import java.util.List;

import com.google.common.base.Joiner;

import client.livy.LivyClient;
import client.livy.Session;
import client.livy.Statement;

public class SimpleApp {

	public static void main(String[] args) {
		// Should be some file on your system
		//String logFile = "/home/marci/build/spark-1.5.0-bin-hadoop2.6/README.md";
		String host = "http://localhost:8998";
		LivyClient client = new LivyClient(host);
//		try {
//			Session session = new Session();
//			session.kind = Kind.SPARK;
//			client.createSession(session);
//			System.out.println(session);
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
		try {
			List<Session> sessions = client.getSessions();
			System.out.println(Joiner.on('\n').join(sessions));
		} catch (IOException e) {
			e.printStackTrace();
		}
//		try {
//			client.deleteSession(11);
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
		try {
			Session session = client.getSessionLogs(0);
			System.out.println(session);
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			Statement statement = client.createStatement(0, "1 + 1");
			System.out.println(statement);
		} catch (IOException e) {
			e.printStackTrace();
		}
		for (int i = 0; i < 100; ++i) {
			try {
				List<Statement> statements = client.getStatements(0);
				System.out.println(Joiner.on('\n').join(statements));
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try {
				Session session = client.getSessionLogs(0);
				System.out.println(session);
			} catch (IOException e) {
				e.printStackTrace();
			}
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

//		try {
//			URL url = new URL("http://www.example.com/customers");
//			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
//			//connection.setDoOutput(true);
//			//connection.setInstanceFollowRedirects(false);
//			connection.setRequestMethod("POST");
//			connection.setRequestProperty("Content-Type", "application/json");
//			r = requests.post(host + "/sessions", data=json.dumps(data), headers=headers)
//			OutputStream os = connection.getOutputStream();
//			jaxbContext.createMarshaller().marshal(customer, os);
//			os.flush();
//
//			connection.getResponseCode();
//			connection.disconnect();
//		} catch(Exception e) {
//			throw new RuntimeException(e);
//		}
//		SparkConf conf = new SparkConf().setAppName("Simple Application");
//		JavaSparkContext sc = new JavaSparkContext(conf);
//		JavaRDD<String> logData = sc.textFile(logFile).cache();
//		@SuppressWarnings("serial")
//		long numAs = logData.filter(new Function<String, Boolean>() {
//
//			@Override
//			public Boolean call(String s) {
//				return s.contains("a");
//			}
//		}).count();
//		@SuppressWarnings("serial")
//		long numBs = logData.filter(new Function<String, Boolean>() {
//
//			@Override
//			public Boolean call(String s) {
//				return s.contains("b");
//			}
//		}).count();
//		System.out.println("Lines with a: " + numAs + ", lines with b: " + numBs);
	}
}
